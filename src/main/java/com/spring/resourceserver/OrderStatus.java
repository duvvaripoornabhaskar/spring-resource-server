package com.spring.resourceserver;

public enum OrderStatus {
 NEW, APPROVED, REJECTED
}
