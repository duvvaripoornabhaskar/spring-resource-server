package com.spring.resourceserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true,securedEnabled = true, jsr250Enabled = true)
public class WebSecurityConfig  { 
	
	@Value(value = "spring.security.oauth2.resourceserver.jwt.issuer-uri")
	private String issuerUri;
	
	@Bean
	SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter(); 
		jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new JwtRoleConverter());
		
		return httpSecurity
		        .csrf(csrf -> csrf.disable())
		        .authorizeHttpRequests(auth -> auth
		        		//.requestMatchers("/orders").hasRole("USER")
		            .anyRequest().authenticated()
		        )
		        .sessionManagement(sess -> sess.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
		        .oauth2ResourceServer(oauth2 -> oauth2.jwt(jwt -> jwt.jwtAuthenticationConverter(jwtAuthenticationConverter).decoder (JwtDecoders.fromIssuerLocation("http://auth-server:8000"))))
		        .httpBasic(Customizer.withDefaults())
		        .build();
	}
}