package com.spring.resourceserver;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class OrdersController {
	
	@GetMapping("/orders")
	//@PreAuthorize("hasRole('USER')")
	@Secured("ROLE_USER")
	public List<OrderRest> getOrders(Authentication authentication) {
		
		//authentication.getAuthorities().forEach(System.out::println);
		
		OrderRest order1 = new OrderRest(UUID.randomUUID().toString(),
				authentication.getName(), "user-id-1", 1, OrderStatus.NEW);
		
		OrderRest order2 = new OrderRest(UUID.randomUUID().toString(),
				authentication.getAuthorities().size()+"", "user-id-1", 1, OrderStatus.NEW);
		
		return Arrays.asList(order1, order2);
	}

}
